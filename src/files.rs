/*
    Copyright (C) 2021 michael mccune

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::{Context, Result};
use std::io::{self, Read};

pub fn get_raw_definition(filename: String) -> Result<String> {
    match filename.as_str() {
        "stdin" => get_definition_from_stdin(),
        _ => get_definition_from_file(filename),
    }
}

fn get_definition_from_stdin() -> Result<String> {
    let mut stdin = io::stdin();
    let mut buffer = String::new();
    stdin.read_to_string(&mut buffer)
        .with_context(|| format!("Failed to read definition from stdin"))?;
    Ok(buffer)
}

fn get_definition_from_file(filename: String) -> Result<String> {
    let buffer = std::fs::read_to_string(&filename)
        .with_context(|| format!("could not read definition file `{}`", filename))?;
    Ok(buffer)
}

pub fn get_template_from_file(filename: String) -> Result<String> {
    let buffer = std::fs::read_to_string(&filename)
        .with_context(|| format!("could not read template file `{}`", filename))?;
    Ok(buffer)
}

/*
    Copyright (C) 2021 michael mccune

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::{Context, Result};
use serde_json::{Value, json};
use serde_json::map::{Map};

pub struct OpenApiDefinition {
    /// A serde_json::Value representation of the OpenApi definition
    pub value: Value,
}

impl OpenApiDefinition {
    pub fn new(input: String) -> Result<OpenApiDefinition> {
        let value: Map<String, Value> = serde_json::from_str(&input)
            .with_context(|| format!("Failed to create OpenApiDefinition, unrecognized json format"))?;
        Ok(OpenApiDefinition{
            value: json!({"openapi": value}),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn error_on_unstructured_input() {
        let input = String::from("{ not valid jason ]");
        match OpenApiDefinition::new(input) {
            Ok(_) => panic!("Expected error got Ok"),
            Err(e) => assert_eq!("Failed to create OpenApiDefinition, unrecognized json format", e.to_string()),
        }
    }
}

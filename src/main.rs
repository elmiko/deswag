/*
    Copyright (C) 2021 michael mccune

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::{Result};
use handlebars::Handlebars;
use std::io::{self, Write};
use structopt::StructOpt;

mod files;
use files::*;
mod openapi;
use openapi::*;

#[derive(StructOpt)]
struct Cli {
    /// The input OpenAPI definition filename or input stream from stdin
    #[structopt(default_value = "stdin")]
    input: String,

    /// The handlebars template filename
    #[structopt(short = "t", long = "template")]
    template: String,
}

fn main() -> Result<()> {
    let args = Cli::from_args();

    let raw = get_raw_definition(args.input)?;
    let template = get_template_from_file(args.template)?;
    let mut reg = Handlebars::new();
    reg.register_template_string("template", template)?;

    let definition = OpenApiDefinition::new(raw)?;
    let output = reg.render("template", &definition.value)?;
    io::stdout().write_all(output.as_bytes())?;
    Ok(())
}

# Convert to YAML

This example shows how the JSON schema could be converted to YAML using the
template methodology. This is **never** recommended for production and is
displayed here to show the flexibility of using deswag.

## Run the example

With the template and OpenAPI schema in the same directory, run the following:

```
deswag --template song-collection.v2.yaml.hbs song-collection.v2.json
```

# deswag examples

These are examples which demonstrate various ways to use deswag with OpenAPI schemas. They
are all based on a fictional song collection API that is defined in the
[`song-collection.v2.json`](song-collection.v2.json) file.

Each subdirectory contains a different example, please see the readme files in those
directories for more information.
